//
//  HelloWorldScene.h
//  dragon
//
//  Created by Владимир on 04.04.15.
//
//

#ifndef __TUTORIAL_H__
#define __TUTORIAL_H__

#include "cocos2d.h"

#include "../Players/Player.h"
#include "MenuScene.h"
#include "../Players/HelperStructs.h"
#include "../Players/Block.h"
#include "../Players/Bullet.h"

class TutorialScene : public cocos2d::Layer {
public:
    TutorialScene();
    ~TutorialScene(){};
    static cocos2d::Scene* createScene(playerInitPack);
    virtual bool init(playerInitPack);
    void update(float);
private:
    /** physics world storage */
    cocos2d::PhysicsWorld* gameWorld;
    /** physics world setter */
    void setWorld(cocos2d::PhysicsWorld* physicsWorld) { gameWorld = physicsWorld; };
    /** main player */
    Player* superPlayer;
    /** UI callback for a "back" button */
    void menuBack(cocos2d::Ref* );
    /** UI callback for a "next" button */
    void nextStep(cocos2d::Ref* );
    /** UI callback to process touches */
    virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
    /** physics callback to process colisions */
    virtual bool onContactBegin(cocos2d::PhysicsContact& );
    /** status bar setter */
    void notifyConnected(std::string nickname);
    /** status bar setter */
    void notifyExited(std::string nickname);
    /** main method to update game status bar */
    void updateStatusBar(std::string action);
    void onKeyPressed(cocos2d::EventKeyboard::KeyCode, cocos2d::Event*);
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode, cocos2d::Event*);
    /** stores last UI notification sent to status bar */
    char* lastAction;
    /** game-round timer */
    double timeSinceStart;
    /** status bar UI label */
    cocos2d::Label* statusBarLabel;
    /** demo printout counter */
    int step;
};

#endif // __TUTORIAL_H__
