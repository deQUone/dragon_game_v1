//
//  MenuSceneConnect.cpp
//  dragon
//
//  Created by Владимир on 01.05.15.
//
//

#include "MenuSceneConnect.h"


MenuSceneConnect::~MenuSceneConnect(){
    openGames.clear();
}


cocos2d::Scene* MenuSceneConnect::createScene(playerInitPack pack, std::vector<playerInitPack> games) {
    auto scene = cocos2d::Scene::create();
    MenuSceneConnect *layer = new(std::nothrow) MenuSceneConnect();
    if (layer && layer->init(pack, games)) {
        layer->autorelease();
        scene->addChild(layer);
        return scene;
    } else {
        delete layer;
        layer = NULL;
        return NULL;
    }
}


bool MenuSceneConnect::init(playerInitPack inpack, std::vector<playerInitPack> games) {
    
    if (!Layer::init()) {
        return false;
    }
    cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
    cocos2d::Vec2 origin = cocos2d::Director::getInstance()->getVisibleOrigin();
    
    // add background
    auto background = cocos2d::Sprite::create("MenuConnect.png");
    background -> setAnchorPoint(cocos2d::Vec2(0, 0));
    background -> setScale(visibleSize.width/background->getContentSize().width, visibleSize.height/background->getContentSize().height);
    this -> addChild(background, -100);
    
    // scene title 
    hintLabel = cocos2d::Label::createWithTTF("choose a game and tap (+)", "fonts/PFIsotextPro-Regular.ttf", 35);
    hintLabel -> setPosition(origin.x + visibleSize.width/2, origin.y + visibleSize.height - 80);
    this -> addChild(hintLabel);
 
    // hard coded container of 5 games only
    auto menu = cocos2d::Menu::create();
    menu -> setPosition(cocos2d::Vec2::ZERO);
    this -> addChild(menu, 1);
    
    for (int i = 0; i < games.size() && i < 5; ++i) {
        std::string text;
        if (std::strcmp((games[i].body)["online"].c_str(), "1") == 0) {
            text = "Online " + (games[i].body)["online"] + " player\nName: \"" + (games[i].body)["name"] + "\"";
        } else {
            text = "Online " + (games[i].body)["online"] + " players\nName: \"" + (games[i].body)["name"] + "\"";
        }
        auto label = cocos2d::Label::createWithTTF(text, "fonts/PFIsotextPro-Regular.ttf", 30);
        label -> setPosition(origin.x + 400 + label -> getContentSize().width/2, origin.y + visibleSize.height - 170 - i*(label -> getContentSize().height + 50));
        this -> addChild(label);
        
        auto playerMap = cocos2d::Sprite::create(playerInitPack::getMap(games[i].chosenMap));
        playerMap -> setScale(150/playerMap -> getContentSize().width, 100/playerMap -> getContentSize().height);
        playerMap -> setPosition(origin.x + 300, origin.y + label -> getPositionY());
        this -> addChild(playerMap);
        
        auto connectButton = cocos2d::MenuItemImage::create("button_plus.png", "button_plus.png", CC_CALLBACK_1(MenuSceneConnect::gameConnetToGame, this));
        connectButton -> setPosition(origin.x + visibleSize.width - 260, origin.y + label -> getPositionY());
        connectButton -> setTag(i);
        menu -> addChild(connectButton);
        
        auto container = cocos2d::Sprite::create("itemContainer.png");
        container -> setPosition(origin.x + visibleSize.width/2, origin.y + playerMap -> getPositionY());
        container -> setScale(670.0/visibleSize.width, container->getContentSize().height/193.0);
        this -> addChild(container, -10);
    }
    
    // back button
    auto backButton = cocos2d::MenuItemImage::create("button_left.png", "button_left.png", CC_CALLBACK_1(MenuSceneConnect::menuBack, this));
    backButton -> setPosition(origin.x + 50, origin.y + visibleSize.height - 50);
    menu -> addChild(backButton);
    
    pack = inpack;
    openGames = games;
    
    this -> scheduleUpdate();
    return true;
}


void MenuSceneConnect::menuBack(cocos2d::Ref* ) {
    cocos2d::Director::getInstance() -> replaceScene(MenuScene::createScene());
}


void MenuSceneConnect::gameConnetToGame(cocos2d::Ref* sender) {
    hintLabel -> setString("waiting...");
    
    auto button = (cocos2d::Sprite* )sender;
    int i = button->getTag();
    pack.publicKey = (openGames[i].body)["publicKey"];
    pack.terrain = openGames[i].terrain;
    pack.blocks = openGames[i].blocks;
    pack.chosenMap = openGames[i].chosenMap;
    Client::getSharedInstance() -> connectToGame(pack);
}


void MenuSceneConnect::update(float dt) {
    int requestStatus = Client::getSharedInstance() -> isRequestDone();
    if (requestStatus == 1) {
        std::vector<playerInitPack> updates = Client::getSharedInstance() -> getUpdates();
        
        int flag = -1;
        for (int i = 0; i < updates.size(); ++i) {
            if (strcmp(updates[i].message.c_str(), "connect") == 0) {
                flag = i;
                break; // connect only to selt
            }
        }
        if (flag != -1) {
            pack.playerID = updates[flag].playerID;
            updates.erase(updates.begin() + flag);
            
            cocos2d::Director::getInstance() -> replaceScene(HelloWorld::createScene(pack, updates));
        } else {
            // I believe there must be only one object if error (no game - no players)
            hintLabel -> setString(updates[0].message);
        }
    }
}
