//
//  HelloWorldScene.cpp
//  dragon
//
//  Created by Владимир on 04.04.15.
//
//

#include "HelloWorldScene.h"


HelloWorld::HelloWorld() {
    step = 1; // local timer for actions
    timeSinceStart = 0.0;
}


HelloWorld::~HelloWorld() {
    opponents.clear();
    opponents_list.clear();
}


/// rewrite default creation method to pass an argument there ///
cocos2d::Scene* HelloWorld::createScene(playerInitPack skin, std::vector<playerInitPack> ops) {
    auto scene = cocos2d::Scene::createWithPhysics();
    scene -> getPhysicsWorld() -> setDebugDrawMask(cocos2d::PhysicsWorld::DEBUGDRAW_NONE);
    HelloWorld *layer = new(std::nothrow) HelloWorld();
    if (layer && layer -> init(skin, ops)) {
        layer -> autorelease();
        layer -> setWorld(scene -> getPhysicsWorld());
        scene -> addChild(layer);
        return scene;
    } else {
        delete layer;
        layer = NULL;
        return NULL;
    }
}


bool HelloWorld::init(playerInitPack skin, std::vector<playerInitPack> ops) {
    if (!Layer::init()) {
        return false;
    }
    cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
    cocos2d::Vec2 origin = cocos2d::Director::getInstance()->getVisibleOrigin();
    
    // create a physics box
    auto edgeBody = cocos2d::PhysicsBody::createEdgeBox(visibleSize, cocos2d::PhysicsMaterial(1, 0.01, 1), 3.5);
    edgeBody -> setDynamic(false);
    auto edgeNode = Node::create();
    edgeNode -> setPosition(cocos2d::Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    edgeNode -> setPhysicsBody(edgeBody);
    this->addChild(edgeNode, 0);
    
    // render phys scene
    cocos2d::Vec2* terr = new cocos2d::Vec2[skin.terrain.size()];
    for (int i = 0; i < skin.terrain.size(); ++i) {
        if (skin.terrain[i].x == -1.0) {
            skin.terrain[i].x = visibleSize.width;
        }
        if (skin.terrain[i].y == -1.0) {
            skin.terrain[i].y = visibleSize.height;
        }
        terr[i] = skin.terrain[i];
    }
    skin.terrain.clear();
    auto ground = cocos2d::PhysicsBody::createEdgePolygon(terr, 4);
    ground -> setDynamic(false);
    auto groundNode = Node::create();
    groundNode -> setAnchorPoint(cocos2d::Point(0, 80));
    groundNode -> setPhysicsBody(ground);
    this -> addChild(groundNode, 0);
    
    const char* blockPics[] = {"Block1.png\0", "Block2.png\0"};
    for (int i = 0; i < skin.blocks.size(); ++i) {
        Block(visibleSize.width/800*skin.blocks[i].x, visibleSize.height/800*skin.blocks[i].y, skin.blocks[i].rot, blockPics[i%2], this);
    }
    skin.blocks.clear();
    
    auto background = cocos2d::Sprite::create(playerInitPack::getMap(skin.chosenMap));
    background -> setAnchorPoint(cocos2d::Vec2(0, 0));
    background -> setScale(visibleSize.width/background->getContentSize().width, visibleSize.height/background->getContentSize().height);
    this -> addChild(background, -100);
    
    // back button
    auto backButton = cocos2d::MenuItemImage::create("button_left.png", "button_left.png", CC_CALLBACK_1(HelloWorld::menuBack, this));
    backButton -> setPosition(origin.x + 50, origin.y + visibleSize.height - 50);
    
    // create menu, it's an autorelease object
    auto menu = cocos2d::Menu::create(backButton, NULL);
    menu -> setPosition(cocos2d::Vec2::ZERO);
    this -> addChild(menu, 1);

    statusBarLabel = cocos2d::Label::createWithTTF("", "fonts/PFIsotextPro-Regular.ttf", 35);
    statusBarLabel -> setPosition(origin.x + visibleSize.width/2, origin.y + visibleSize.height - 55);
    this -> addChild(statusBarLabel, 2);
    updateStatusBar("Dragon game 1.0");

    // init yourself
    superPlayer = new Player(skin, this);
    
    // init opponents (if connect to existing game)
    for (int i = 0; i < ops.size(); ++i) {
        addOpponent(ops[i], this);
    }

    // init touch listener
    auto touchListener = cocos2d::EventListenerTouchOneByOne::create();
    touchListener -> onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
    _eventDispatcher -> addEventListenerWithSceneGraphPriority(touchListener, this);
    
    // init physic event listener
    auto contactListener = cocos2d::EventListenerPhysicsContact::create();
    contactListener -> onContactBegin = CC_CALLBACK_1(HelloWorld::onContactBegin, this);
    _eventDispatcher -> addEventListenerWithSceneGraphPriority(contactListener, this);
    
    auto listener = cocos2d::EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(HelloWorld::onKeyPressed, this);
    listener->onKeyReleased = CC_CALLBACK_2(HelloWorld::onKeyReleased, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    // run the Controller Cycle
    this -> scheduleUpdate();
    return true;
}

void HelloWorld::onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) {
    //printf("Key with keycode %d pressed", keyCode);
    if ((int)keyCode == 59) {
        //superPlayer -> attack();
        printf("Attack inited by superPlayer");
    }
}

void HelloWorld::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) {
	//cocos2d::Vec2 touchPos = touch -> getLocation();
    //superPlayer -> change_pos(touchPos);
    //Client::getSharedInstance() -> sendUpdates(superPlayer -> getPack()); // must contain curent pos(-) + target pos(+)
    printf("Key with keycode %d released", keyCode);
}


// use to update game score
bool HelloWorld::onContactBegin(cocos2d::PhysicsContact& contact) {
    //auto bodyA = contact.getShapeA()->getBody();
    //auto bodyB = contact.getShapeB()->getBody();
    printf("onContactBegin...\n");
    return true;
}


// simple create an object + save it
void HelloWorld::addOpponent(playerInitPack pack, cocos2d::Layer* layer) {
    Player* new_opponent = new Player(pack, layer);
    opponents_list.push_back(pack.playerID);
    opponents[pack.playerID] = new_opponent;
}


#define ROUND 120.0

void HelloWorld::update(float dt) {
    //// - fix label movement:
    superPlayer -> move_label();
    for (int i = 0; i < opponents_list.size(); ++i) {
        opponents[opponents_list[i]] -> move_label();
    }

    //// - implement updates for existing objects
    int requestStatus = Client::getSharedInstance() -> isRequestDone();
    if (requestStatus == 1) {
        std::vector<playerInitPack> updates = Client::getSharedInstance() -> getUpdates();
        for (int i = 0; i < updates.size(); ++i) {
            if (strcmp(updates[i].message.c_str(), "create") == 0) {
                // no player -> create player
                addOpponent(updates[i], this);
                notifyConnected(updates[i].nickname);
            } else if (strcmp(updates[i].message.c_str(), "move") == 0) {
                // fix pos (add server field + struct field)
                opponents[updates[i].playerID] -> fix_pos(updates[i].position);
                opponents[updates[i].playerID] -> change_pos(updates[i].target);
            } else if (strcmp(updates[i].message.c_str(), "exit") == 0) {
                // remove player from view and from our storages
                notifyExited(opponents[updates[i].playerID] -> getPack().nickname);
                opponents[updates[i].playerID] -> remove();
                opponents.erase(updates[i].playerID);
                opponents_list.erase(std::remove(opponents_list.begin(), opponents_list.end(), updates[i].playerID));
            } else {
                printf("no processor for %s-method\n", updates[i].message.c_str());
            }
        }
    } else if (requestStatus != -2 && step % 5 == 0) {
        Client::getSharedInstance() -> fetchUpdates(superPlayer -> getPack());
    }
    step++ ;
    timeSinceStart += dt;
    if (timeSinceStart < ROUND) {
        updateStatusBar("");
    } else {
        this -> pause();
        
        // + send request like exit but with results and for all players!
        Client::getSharedInstance() -> exitGame(superPlayer -> getPack());
        
        updateStatusBar("Round over!");
    }
}


// left part
void HelloWorld::notifyConnected(std::string nick) {
    updateStatusBar("Connected: \"" + nick + "\"");
}


void HelloWorld::notifyExited(std::string nick) {
    updateStatusBar("\"" + nick + "\" leaved game");
}


// status bar render: bring new or old action to view
void HelloWorld::updateStatusBar(std::string action) {
    std::string statusBarString = "";
    if (action.size() > 0) {
        statusBarString += action;
        lastAction = (char* ) malloc(action.size());
        strcpy(lastAction, action.c_str());
    } else {
        statusBarString += lastAction;
    }
    statusBarString += "          ";
    char buf[10];
    sprintf(buf, "%.1f", timeSinceStart);
    statusBarString += buf;
    statusBarString += " sec / ";
    sprintf(buf, "%.0f", ROUND);
    statusBarString += buf;
    statusBarString += " sec total";
    statusBarLabel -> setString(statusBarString);
}


// this method updates only Superplayer
bool HelloWorld::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) {
    cocos2d::Vec2 touchPos = touch -> getLocation();
    superPlayer -> change_pos(touchPos);
    Client::getSharedInstance() -> sendUpdates(superPlayer -> getPack()); // must contain curent pos(-) + target pos(+)
    return true;
}


// back in menu
void HelloWorld::menuBack(cocos2d::Ref* ) {
    Client::getSharedInstance() -> exitGame(superPlayer -> getPack());
    cocos2d::Director::getInstance() -> replaceScene(MenuScene::createScene());
}
