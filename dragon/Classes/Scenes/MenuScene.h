//
//  MenuScene.h
//  dragon
//
//  Created by Владимир on 27.04.15.
//
//

#ifndef __MENU_SCENE_H__
#define __MENU_SCENE_H__

#include <iostream>
#include <stdio.h>
#include <string.h>

#include "cocos2d.h"
#include "ui/UITextField.h"

#include "../Players/HelperStructs.h"
#include "../Client.h"
#include "MenuSceneNew.h"
#include "MenuSceneConnect.h"
#include "TutorialScene.h"

class MenuScene : public cocos2d::Layer {
public:
    MenuScene();
    ~MenuScene();
    static cocos2d::Scene* createScene();
    virtual bool init();
    /** server calback handler method */
    void update(float);
    CREATE_FUNC(MenuScene);
private:
    /** stores the result of text input */
    char* nickname;
    /** UI form for text input */
    cocos2d::ui::TextField* textInput;
    /** UI label for system messages */
    cocos2d::Label* hintLabel;
    /** stores slider contents */
    std::vector<playerInitPack> startPacks;
    /** number of current active skin */
    int chosen;
    /** current active skin picture */
    cocos2d::Sprite* playerImage;
    /** stores chosen pack */
    playerInitPack start;
    /** UI callback for a "left" button */
    void sliderLeft(cocos2d::Ref* );
    /** UI callback for a "right" button */
    void sliderRight(cocos2d::Ref* );
    /** processes all user input */
    bool prepareStartPack();
    /** UI callback for a "new game" button */
    void gameStartNewGame(cocos2d::Ref* );
    /** UI callback for a "connect" button */
    void gameConnetToGame(cocos2d::Ref* );
    /** UI callback for a "tutorial" button */
    void gameTutorial(cocos2d::Ref* );
    /** UI callback for a "close" button */
    void menuCloseCallback(cocos2d::Ref* );
};

#endif // __MENU_SCENE_H__
