//
//  MenuSceneConnect.h
//  dragon
//
//  Created by Владимир on 01.05.15.
//
//

#ifndef __MENU_SCENE_CONNECT_H__
#define __MENU_SCENE_CONNECT_H__

#include <iostream>
#include <sstream>
#include <string.h>

#include "cocos2d.h"
#include "ui/UIScrollView.h"

#include "../Players/HelperStructs.h"
#include "../Client.h"
#include "MenuScene.h" // back...
#include "HelloWorldScene.h" // our game action there

class MenuSceneConnect : public cocos2d::Layer {
public:
    MenuSceneConnect(){};
    ~MenuSceneConnect();
    static cocos2d::Scene* createScene(playerInitPack, std::vector<playerInitPack>);
    virtual bool init(playerInitPack, std::vector<playerInitPack>);
    /** server calback handler method */
    void update(float dt);
private:
    /** stores chosen skin and nickname */
    playerInitPack pack;
    /** stores data about open games */
    std::vector<playerInitPack> openGames;
    /** UI label for system messages */
    cocos2d::Label* hintLabel;
    /** UI callback for a "(+)" button */
    void gameConnetToGame(cocos2d::Ref* );
    /** UI callback for a "back" button */
    void menuBack(cocos2d::Ref* );
    /** UI callback for a "close" button */
    void menuCloseCallback(cocos2d::Ref* );
};

#endif // __MENU_SCENE_CONNECT_H__
