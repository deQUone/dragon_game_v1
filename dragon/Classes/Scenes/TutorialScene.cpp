//
//  HelloWorldScene.cpp
//  dragon
//
//  Created by Владимир on 04.04.15.
//
//

#include "TutorialScene.h"


TutorialScene::TutorialScene() {
    step = 1; // local timer for actions
    timeSinceStart = 0.0;
}


/// rewrite default creation method to pass an argument there ///
cocos2d::Scene* TutorialScene::createScene(playerInitPack skin) {
    auto scene = cocos2d::Scene::createWithPhysics();
    scene -> getPhysicsWorld() -> setDebugDrawMask(cocos2d::PhysicsWorld::DEBUGDRAW_ALL);
    TutorialScene *layer = new(std::nothrow) TutorialScene();
    if (layer && layer -> init(skin)) {
        layer -> autorelease();
        layer -> setWorld(scene -> getPhysicsWorld());
        scene -> addChild(layer);
        return scene;
    } else {
        delete layer;
        layer = NULL;
        return NULL;
    }
}


bool TutorialScene::init(playerInitPack skin) {
    if (!Layer::init()) {
        return false;
    }
    cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
    cocos2d::Vec2 origin = cocos2d::Director::getInstance()->getVisibleOrigin();
    
    auto edgeBody = cocos2d::PhysicsBody::createEdgeBox(visibleSize, cocos2d::PhysicsMaterial(10, 0.01, 1), 3.5);
    edgeBody -> setDynamic(false);
    auto edgeNode = Node::create();
    edgeNode -> setPosition(cocos2d::Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    edgeNode -> setPhysicsBody(edgeBody);
    this -> addChild(edgeNode, 0);
    
    cocos2d::Vec2* points = new cocos2d::Vec2[4];       //
    points[0] = cocos2d::Vec2(0, 0);                    //
    points[1] = cocos2d::Vec2(0, 80);                   //  actually server sends this data to each client
    points[2] = cocos2d::Vec2(visibleSize.width, 60);   //
    points[3] = cocos2d::Vec2(visibleSize.width, 0);    //
    auto ground = cocos2d::PhysicsBody::createEdgePolygon(points, 4);
    ground -> setDynamic(false);
    auto groundNode = Node::create();
    groundNode -> setAnchorPoint(cocos2d::Point(0, 80));
    groundNode -> setPhysicsBody(ground);
    this -> addChild(groundNode, 0);
    
    Block(400, 400, 50, "Block1.png\0", this);
    Block(visibleSize.width - 300, 200, 150, "Block2.png\0", this);
    
    auto background = cocos2d::Sprite::create("Scene1.png");
    background -> setAnchorPoint(cocos2d::Vec2(0, 0));
    background -> setScale(visibleSize.width/background->getContentSize().width, visibleSize.height/background->getContentSize().height);
    this -> addChild(background, -100);
    
    // back button
    auto backButton = cocos2d::MenuItemImage::create("button_left.png", "button_left.png", CC_CALLBACK_1(TutorialScene::menuBack, this));
    backButton -> setPosition(origin.x + 50, origin.y + visibleSize.height - 50);
    
    auto nextButton = cocos2d::MenuItemImage::create("button_next.png", "button_next.png", CC_CALLBACK_1(TutorialScene::nextStep, this));
    nextButton -> setPosition(visibleSize.width/2, 40);
    
    // create menu, it's an autorelease object
    auto menu = cocos2d::Menu::create(backButton, nextButton, NULL);
    menu -> setPosition(cocos2d::Vec2::ZERO);
    this -> addChild(menu, 1);

    statusBarLabel = cocos2d::Label::createWithTTF("", "fonts/PFIsotextPro-Regular.ttf", 35);
    statusBarLabel -> setPosition(origin.x + visibleSize.width/2, origin.y + visibleSize.height - 55);
    this -> addChild(statusBarLabel, 2);
    updateStatusBar("Welcome to Tutorial!");

    // init yourself
    superPlayer = new Player(skin, this);
    
    // init touch listener
    auto touchListener = cocos2d::EventListenerTouchOneByOne::create();
    touchListener -> onTouchBegan = CC_CALLBACK_2(TutorialScene::onTouchBegan, this);
    _eventDispatcher -> addEventListenerWithSceneGraphPriority(touchListener, this);
    
    // init physic event listener
    auto contactListener = cocos2d::EventListenerPhysicsContact::create();
    contactListener -> onContactBegin = CC_CALLBACK_1(TutorialScene::onContactBegin, this);
    _eventDispatcher -> addEventListenerWithSceneGraphPriority(contactListener, this);
    
    
    auto listener = cocos2d::EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(TutorialScene::onKeyPressed, this);
    listener->onKeyReleased = CC_CALLBACK_2(TutorialScene::onKeyReleased, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    
    // run the Controller Cycle
    this -> scheduleUpdate();
    return true;
}

void TutorialScene::onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) {
    if ((int)keyCode == 59) {
    	    superPlayer -> attack();
    }
}

void TutorialScene::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) {
}


// use to update game score
bool TutorialScene::onContactBegin(cocos2d::PhysicsContact& contact) {
    printf("onContactBegin...\n");
    return true;
}


#define ROUND 30.0

void TutorialScene::update(float dt) {
    superPlayer -> move_label();
    
    //timeSinceStart += dt; // run it after button
    if (timeSinceStart < ROUND) {
        updateStatusBar("");
    } else {
        this -> pause();
        updateStatusBar("Round over!");
    }
}


// left part
void TutorialScene::notifyConnected(std::string nick) {
    updateStatusBar("Connected: \"" + nick + "\"");
}


void TutorialScene::notifyExited(std::string nick) {
    updateStatusBar("\"" + nick + "\" leaved game");
}


// status bar processing
void TutorialScene::updateStatusBar(std::string action) {
    std::string statusBarString = "";
    if (action.size() > 0) {
        statusBarString += action;
        lastAction = (char* ) malloc(action.size());
        strcpy(lastAction, action.c_str());
    } else {
        statusBarString += lastAction;
    }
    statusBarString += "          ";
    char buf[10];
    sprintf(buf, "%.1f", timeSinceStart);
    statusBarString += buf;
    statusBarString += " sec / ";
    sprintf(buf, "%.0f", ROUND);
    statusBarString += buf;
    statusBarString += " sec total";
    statusBarLabel -> setString(statusBarString);
}


// move Superplayer
bool TutorialScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) {
    superPlayer -> change_pos(touch -> getLocation());
    return true;
}


void TutorialScene::nextStep(cocos2d::Ref* ) {
    step += 1;
    
    
}


void TutorialScene::menuBack(cocos2d::Ref* ) {
    cocos2d::Director::getInstance() -> replaceScene(MenuScene::createScene());
}
