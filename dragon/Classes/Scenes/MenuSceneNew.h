//
//  MenuScene.h
//  dragon
//
//  Created by Владимир on 27.04.15.
//
//

#ifndef __MENU_SCENE_NEW_H__
#define __MENU_SCENE_NEW_H__

#include <iostream>
#include <stdio.h>
#include <string.h>

#include "cocos2d.h"
#include "ui/UITextField.h"

#include "../Players/HelperStructs.h"
#include "../Client.h"
#include "MenuScene.h"
#include "HelloWorldScene.h" // our game action there

class MenuSceneNew : public cocos2d::Layer {
public:
    MenuSceneNew();
    ~MenuSceneNew();
    static cocos2d::Scene* createScene(playerInitPack);
    virtual bool init(playerInitPack);
    void update(float);
private:
    /** stores chosen pack */
    playerInitPack start;
    /** stores the result of text input */
    char* gameName;
    /** UI form for text input */
    cocos2d::ui::TextField* textInput;
    /** UI label for system messages */
    cocos2d::Label* hintLabel;
    /** content of map-slider */
    std::vector<const char* > gameMaps;
    /** UI callback for a "left" button */
    void sliderMapLeft(cocos2d::Ref* );
    /** UI callback for a "right" button */
    void sliderMapRight(cocos2d::Ref* );
    /** number of current active map */
    int chosenMap;
    /** current active skin picture */
    cocos2d::Sprite* playerMap;
    /** processes all user input */
    bool prepareStartPack();
    /** UI callback for a "new game" button */
    void gameStartNewGame(cocos2d::Ref* );
    /** UI callback for a "back" button */
    void menuBack(cocos2d::Ref* );
};

#endif // __MENU_SCENE_NEW_H__
