//
//  MenuSceneNew.cpp
//  dragon
//
//  Created by Владимир on 14.05.15.
//
//

#include "MenuSceneNew.h"


MenuSceneNew::MenuSceneNew() {
    gameName = (char* ) malloc(1);
}


MenuSceneNew::~MenuSceneNew() {
    delete gameName;
}

cocos2d::Scene* MenuSceneNew::createScene(playerInitPack pack) {
    auto scene = cocos2d::Scene::create();
    MenuSceneNew *layer = new(std::nothrow) MenuSceneNew();
    if (layer && layer->init(pack)) {
        layer->autorelease();
        scene->addChild(layer);
        return scene;
    } else {
        delete layer;
        layer = NULL;
        return NULL;
    }
}

#define BASE_OFFSET 90 // UI vertical offset

bool MenuSceneNew::init(playerInitPack inpack) {
    if (!Layer::init()) {
        return false;
    }
    cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
    cocos2d::Vec2 origin = cocos2d::Director::getInstance()->getVisibleOrigin();
    
    // add background
    auto background = cocos2d::Sprite::create("MenuConnect.png");
    background -> setAnchorPoint(cocos2d::Vec2(0, 0));
    background -> setScale(visibleSize.width/background->getContentSize().width, visibleSize.height/background->getContentSize().height);
    this -> addChild(background, -100);
    
//    auto container = cocos2d::Sprite::create("itemContainer.png");
//    container -> setPosition(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2);
//    container -> setScale(800.0/visibleSize.width, 2400.0/visibleSize.height);
//    this -> addChild(container, -10);
    
    // add help label
    hintLabel = cocos2d::Label::createWithTTF("Choose a game map", "fonts/PFIsotextPro-Regular.ttf", 35);
    hintLabel -> setPosition(origin.x + visibleSize.width/2, origin.y + visibleSize.height - 95);
    this -> addChild(hintLabel, -5);
    
    start = inpack;
    
    // init game maps
    chosenMap = 0;
    gameMaps.push_back("Scene1.png");
    gameMaps.push_back("Scene2.png");
    playerMap = cocos2d::Sprite::create(gameMaps[chosenMap]);
    playerMap -> setScale(450/playerMap -> getContentSize().width, 300/playerMap -> getContentSize().height);
    playerMap -> setPosition(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2 + BASE_OFFSET - 60);
    this -> addChild(playerMap, -4);
    
    // init slider for maps
    auto slideLeft = cocos2d::MenuItemImage::create("button_left.png", "button_left.png", "button_left.png", CC_CALLBACK_1(MenuSceneNew::sliderMapLeft, this));
    slideLeft -> setPosition(origin.x + visibleSize.width/2 - 270, origin.y + visibleSize.height/2 + BASE_OFFSET - 90);
    
    auto slideRight = cocos2d::MenuItemImage::create("button_right.png", "button_right.png", "button_right.png", CC_CALLBACK_1(MenuSceneNew::sliderMapRight, this));
    slideRight -> setPosition(origin.x + visibleSize.width/2 + 270, origin.y + visibleSize.height/2 + BASE_OFFSET - 90);
    
    // add input form for nickname
    textInput = cocos2d::ui::TextField::create("Enter game name", "fonts/PFIsotextPro-Regular.ttf", 30);
    textInput -> setPosition(cocos2d::Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2 + BASE_OFFSET - 250));
    textInput -> setMaxLengthEnabled(true);
    textInput -> setMaxLength(10);
    textInput -> addTouchEventListener([&](cocos2d::Ref *target, cocos2d::ui::Widget::TouchEventType type){
        hintLabel -> setString("Choose a game map");
    });
    this -> addChild(textInput, -3);
    
    auto buttonStart = cocos2d::MenuItemImage::create("button_start.png", "button_start.png", "button_start.png", CC_CALLBACK_1(MenuSceneNew::gameStartNewGame, this));
    buttonStart -> setScale(240/buttonStart->getContentSize().width, 65/buttonStart->getContentSize().height);
    buttonStart -> setPosition(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2 + BASE_OFFSET - 310);
    
    // back button
    auto backButton = cocos2d::MenuItemImage::create("button_left.png", "button_left.png", CC_CALLBACK_1(MenuSceneNew::menuBack, this));
    backButton -> setPosition(origin.x + 50, origin.y + visibleSize.height - 50);
    
    auto menu = cocos2d::Menu::create(slideLeft, slideRight, backButton, buttonStart, NULL);
    menu -> setPosition(cocos2d::Vec2::ZERO);
    this -> addChild(menu, -2);
    
    // run the Controller Cycle
    this -> scheduleUpdate();
    return true;
}


void MenuSceneNew::update(float dt) {
    int requestStatus = Client::getSharedInstance() -> isRequestDone();
    if (requestStatus == 1) {
        std::vector<playerInitPack> updates = Client::getSharedInstance() -> getUpdates();
        if (updates.size() > 0) {
            // "new_game", "list" are only allowed on this scene
            if (strcmp(updates[0].message.c_str(), "new_game") == 0) {
                printf("Staring new game...\n");
                
                start.publicKey = updates[0].publicKey;
                start.playerID = updates[0].playerID;
                start.terrain = updates[0].terrain;
                start.blocks = updates[0].blocks;
                
                std::vector<playerInitPack> opponents;
                cocos2d::Director::getInstance() -> replaceScene(HelloWorld::createScene(start, opponents));
            }
        } else {
            hintLabel -> setString("There are no active games at the moment");
        }
    } else if (requestStatus == -1) {
        hintLabel -> setString("Sorry, there are some problems with network...");
    }
}


void MenuSceneNew::sliderMapLeft(cocos2d::Ref* ) {
    chosenMap-- ;
    playerMap -> setTexture(gameMaps[chosenMap%gameMaps.size()]);
}


void MenuSceneNew::sliderMapRight(cocos2d::Ref* ) {
    chosenMap++ ;
    playerMap -> setTexture(gameMaps[chosenMap%gameMaps.size()]);
}


void MenuSceneNew::menuBack(cocos2d::Ref* ) {
    cocos2d::Director::getInstance() -> replaceScene(MenuScene::createScene());
}


void MenuSceneNew::gameStartNewGame(cocos2d::Ref* ) {
    if (prepareStartPack() && Client::getSharedInstance() -> isRequestDone() != -2) {
        Client::getSharedInstance() -> startNewGame(start);
    }
}

bool MenuSceneNew::prepareStartPack() {
    auto text_itself = textInput -> getString();
    if (text_itself.length() == 0) {
        hintLabel -> setString("Please enter game name below!");
        return false;
    } else {
        gameName = (char* ) malloc(text_itself.size());
        memcpy(gameName, text_itself.c_str(), text_itself.size());
        start.gameName = gameName;
        start.chosenMap = chosenMap%gameMaps.size();
        return true;
    }
}

