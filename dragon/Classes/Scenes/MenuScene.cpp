//
//  MenuScene.cpp
//  dragon
//
//  Created by Владимир on 27.04.15.
//
//

#include "MenuScene.h"


MenuScene::MenuScene() {
    startPacks = playerInitPack::defaultInitStuff();
    nickname = (char* ) malloc(1);
}


MenuScene::~MenuScene() {
    delete nickname;
    startPacks.clear();
}


cocos2d::Scene* MenuScene::createScene() {
    auto scene = cocos2d::Scene::create(); // 'scene' is an autorelease object
    auto layer = MenuScene::create(); // 'layer' is an autorelease object
    scene->addChild(layer);
    return scene;
}


#define BASE_OFFSET 110 // UI vertical offset

bool MenuScene::init() {
    if (!Layer::init()) {
        return false;
    }
    cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
    cocos2d::Vec2 origin = cocos2d::Director::getInstance()->getVisibleOrigin();
    
    // add background
    auto background = cocos2d::Sprite::create("MenuIntro.png");
    background -> setAnchorPoint(cocos2d::Vec2(0, 0));
    background -> setScale(visibleSize.width/background->getContentSize().width, visibleSize.height/background->getContentSize().height);
    this -> addChild(background, -100);

    auto container = cocos2d::Sprite::create("itemContainer.png");
    container -> setPosition(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2 + BASE_OFFSET - 13);
    container -> setScale(360.0/visibleSize.width, 700.0/visibleSize.height);
    this -> addChild(container, -80);
    
    // add help label
    hintLabel = cocos2d::Label::createWithTTF("Choose your skin and start a game", "fonts/PFIsotextPro-Regular.ttf", 35);
    hintLabel -> setPosition(origin.x + visibleSize.width/2, origin.y + visibleSize.height - 95);
    this -> addChild(hintLabel, -5);
    
    // init player skin
    chosen = 0;
    playerImage = cocos2d::Sprite::create(startPacks[chosen].body["left"]);
    playerImage -> setPosition(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2 + BASE_OFFSET);
    this -> addChild(playerImage, -4);
    
    // init slider for player skins
    auto slideLeft = cocos2d::MenuItemImage::create("button_left.png", "button_left.png", "button_left.png", CC_CALLBACK_1(MenuScene::sliderLeft, this));
    slideLeft -> setPosition(origin.x + visibleSize.width/2 - 100, origin.y + visibleSize.height/2 + BASE_OFFSET);
    
    auto slideRight = cocos2d::MenuItemImage::create("button_right.png", "button_right.png", "button_right.png", CC_CALLBACK_1(MenuScene::sliderRight, this));
    slideRight -> setPosition(origin.x + visibleSize.width/2 + 100, origin.y + visibleSize.height/2 + BASE_OFFSET);
    
    // add input form for nickname
    textInput = cocos2d::ui::TextField::create("Enter your nick", "fonts/PFIsotextPro-Regular.ttf", 30);
    textInput -> setPosition(cocos2d::Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2 + BASE_OFFSET - 70));
    textInput -> setMaxLengthEnabled(true);
    textInput -> setMaxLength(10);
    textInput -> addTouchEventListener([&](cocos2d::Ref *target, cocos2d::ui::Widget::TouchEventType type){
        hintLabel -> setString("Choose your skin and start a game");
    });
    this -> addChild(textInput, -3);
    
    // add buttons
    auto buttonTutorial = cocos2d::MenuItemImage::create("button_tutorial.png", "button_tutorial.png", "button_connect.png", CC_CALLBACK_1(MenuScene::gameTutorial, this));
    buttonTutorial -> setScale(250/buttonTutorial->getContentSize().width, 66/buttonTutorial->getContentSize().height);
    buttonTutorial -> setPosition(origin.x + visibleSize.width/2 - 250, origin.y + visibleSize.height/2 + BASE_OFFSET - 160);
    
    auto buttonStart = cocos2d::MenuItemImage::create("button_start.png", "button_start.png", "button_start.png", CC_CALLBACK_1(MenuScene::gameStartNewGame, this));
    buttonStart -> setScale(240/buttonStart->getContentSize().width, 65/buttonStart->getContentSize().height);
    buttonStart -> setPosition(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2 + BASE_OFFSET - 160);
    
    auto buttonConnect = cocos2d::MenuItemImage::create("button_connect.png", "button_connect.png", "button_connect.png", CC_CALLBACK_1(MenuScene::gameConnetToGame, this));
    buttonConnect -> setScale(250/buttonConnect->getContentSize().width, 66/buttonConnect->getContentSize().height);
    buttonConnect -> setPosition(origin.x + visibleSize.width/2 + 250, origin.y + visibleSize.height/2 + BASE_OFFSET - 160);
    
    auto menu = cocos2d::Menu::create(slideLeft, slideRight, buttonStart, buttonConnect, buttonTutorial, NULL);
    menu -> setPosition(cocos2d::Vec2::ZERO);
    this -> addChild(menu, -2);
    
    // add help label
    auto howToPlay = cocos2d::Label::createWithTTF("How to play: tap somewhere on the screen \nto do your dragon move.\nHit enemy dragons as much as you can.\nYou have only 30 seconds. Good luck!", "fonts/PFIsotextPro-Regular.ttf", 35);
    howToPlay -> setPosition(origin.x + visibleSize.width/2, origin.y + 125);
    this -> addChild(howToPlay, -1);
    
    // run the Controller Cycle
    this -> scheduleUpdate();
    return true;
}


void MenuScene::update(float dt) {
    int requestStatus = Client::getSharedInstance() -> isRequestDone(); // we have to save current state cause if will null after read
    if (requestStatus == 1) {
        std::vector<playerInitPack> updates = Client::getSharedInstance() -> getUpdates();
        if (updates.size() > 0) {
            if (strcmp(updates[0].message.c_str(), "list") == 0) {
                cocos2d::Director::getInstance() -> replaceScene(MenuSceneConnect::createScene(start, updates));
            }
        } else {
            hintLabel -> setString("No active games at the moment");
        }
    } else if (requestStatus == -1) {
        hintLabel -> setString("Sorry, there are problems with network...");
    }
}


void MenuScene::sliderLeft(cocos2d::Ref* ) {
    chosen-- ;
    playerImage -> setTexture(startPacks[chosen%startPacks.size()].body["left"]);
}


void MenuScene::sliderRight(cocos2d::Ref* ) {
    chosen++ ;
    playerImage -> setTexture(startPacks[chosen%startPacks.size()].body["left"]);
}


bool MenuScene::prepareStartPack() {
    auto text_itself = textInput -> getString();
    if (text_itself.length() == 0) {
        hintLabel -> setString("Please enter nickname below!");
        return false;
    } else {
        nickname = (char* ) malloc(text_itself.size());
        memcpy(nickname, text_itself.c_str(), text_itself.size());
        
        start = startPacks[chosen%startPacks.size()];
        start.chosenSkin = chosen%startPacks.size();
        start.nickname = strdup(nickname);
        return true;
    }
}


void MenuScene::gameTutorial(cocos2d::Ref* ) {
    if (prepareStartPack()) {
        cocos2d::Director::getInstance() -> replaceScene(TutorialScene::createScene(start));
    }
}


void MenuScene::gameStartNewGame(cocos2d::Ref* ) {
    if (prepareStartPack() && Client::getSharedInstance() -> isRequestDone() != -2) {
        cocos2d::Director::getInstance() -> replaceScene(MenuSceneNew::createScene(start));
    }
}


void MenuScene::gameConnetToGame(cocos2d::Ref* ) {
    if (prepareStartPack() && Client::getSharedInstance() -> isRequestDone() != -2) {
        Client::getSharedInstance() -> listOfGames();
    }
}
