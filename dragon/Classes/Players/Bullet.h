//
//  Bullet.h
//  dragon
//
//  Created by Владимир on 16.05.15.
//
//

#ifndef __BULLET__
#define __BULLET__

#include "cocos2d.h"

#include "HelperStructs.h"

class Bullet: public cocos2d::Sprite {
public:
    Bullet(cocos2d::Vec2, cocos2d::Layer* );
    ~Bullet();
    /** life cycle end */
    void remove_self(cocos2d::Layer* );
    /** moving bullet **/
    void move(cocos2d::Vec2, cocos2d::Layer*);
    /** check blocks **/
    void check_blocks(cocos2d::Layer*);
private:
    /** get movement vector */
    cocos2d::Vec2 count_vector(cocos2d::Vec2, cocos2d::Vec2);
    /** place Bullet on scene */
    void create(const cocos2d::Vec2, const cocos2d::Vec2, cocos2d::Layer* );
    /** stores visible body */
    cocos2d::Sprite* body;
    /** stores physical body */
    cocos2d::PhysicsBody* realBody;
};

#endif /* defined(__BULLET__) */
