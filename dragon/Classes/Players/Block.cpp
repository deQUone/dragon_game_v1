//
//  bot.cpp
//  dragon
//
//  Created by Владимир on 01.04.15.
//
//

#include "Block.h"

#define RADIUS_SCALE 112

Block::Block(int x, int y, int rot, const char* filename, cocos2d::Layer* gameScene) {
    auto blockBody = cocos2d::Sprite::create(filename);
    blockBody -> setPosition(cocos2d::Vec2(x, y));
    blockBody -> setRotation(rot);
    blockBody -> setScale(RADIUS_SCALE/blockBody -> getContentSize().width, RADIUS_SCALE/blockBody -> getContentSize().height);
    
    auto realBody = cocos2d::PhysicsBody::createCircle(RADIUS_SCALE/2, cocos2d::PhysicsMaterial(10, 0.1, 0.1));
    realBody -> setDynamic(false);
    
    blockBody -> setPhysicsBody(realBody);
    gameScene -> addChild(blockBody, -3);
}
