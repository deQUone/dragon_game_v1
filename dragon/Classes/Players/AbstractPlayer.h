//
//  AbstractPlayer.h
//  dragon
//
//  Created by Владимир on 18.04.15.
//
//

#ifndef __dragon__AbstractPlayer__
#define __dragon__AbstractPlayer__

#include "cocos2d.h"

#include "HelperStructs.h"
#include "Bullet.h"


class AbstractPlayer: public cocos2d::Sprite {
public:
    AbstractPlayer(playerInitPack, cocos2d::Layer* );
    ~AbstractPlayer();
    /** return a current position of a main body */
    cocos2d::Vec2 get_position();
    /** move all storing bodies to a speciefic position */
    void move(int , int );
    /** set all storing bodies on a speciefic position */
    void set(cocos2d::Vec2);
    /** remove all storing bodies from game scene */
    void remove_self(cocos2d::Layer* );
    /** fix label movement */
    void fix_label_pos();
    /** perform an attack */
    void attack(cocos2d::Layer* );
    /** experiments with new messaging system - send velocity + rotation */
    playerInitPack get_parameters();
    void apply_updates(playerInitPack);
private:
    void create(const cocos2d::Vec2, const std::string, cocos2d::Layer* );
    /** stores player info label */
    cocos2d::Label* infoLabel;
    /** stores last action for sprite */
    cocos2d::MoveTo* lastAction;
    /** stores visible body */
    cocos2d::Sprite* body;
    /** stores physical body */
    cocos2d::PhysicsBody* realBody;
    /** stores target coordinate */
    cocos2d::Vec2 target;
    /** get movement vector */
    cocos2d::Vec2 count_vector(cocos2d::Vec2, cocos2d::Vec2);
    /** 
     @brief     stores last direction to update visible body view
     @return 1      moving right
     @return -1     moving left
     */
    int vector_x;
    /**
     @brief     stores last direction to update visible body view
     @return 1      moving up
     @return -1     moving down
     */
    int vector_y;
    /** physic body velocity */
    int velocity;
    /** player nickname storage */
    std::string nickname;
    /** body sprites storage */
    std::map<std::string, std::string> bodyStorage;
};

#endif /* defined(__dragon__AbstractPlayer__) */
