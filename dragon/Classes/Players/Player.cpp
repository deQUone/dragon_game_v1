//
//  player.cpp
//  dragon
//
//  Created by Владимир on 28.03.15.
//
//

#include "Player.h"


Player::Player(playerInitPack startPack, cocos2d::Layer* gameScene) {
    pack = startPack; // stores all game logic stuff...
    gameWorld = gameScene;
    cocosPlayer = new AbstractPlayer(pack, gameScene);
}


Player::~Player() {
    delete cocosPlayer;
}


void Player::change_pos(cocos2d::Vec2 pos) {
    pack.message = strdup("move"); // other actions will have their own messages
    pack.position = cocosPlayer -> get_position();
    pack.target = pos; // target pos
    cocosPlayer -> move(pos.x, pos.y);
}


void Player::attack() {
    cocosPlayer -> attack(gameWorld);
}


void Player::move_label() {
    cocosPlayer -> fix_label_pos();
}


void Player::fix_pos(cocos2d::Vec2 pos) {
    pack.position = pos;
    cocosPlayer -> set(pos);
}


void Player::simulate_movement(playerInitPack fixPack) {
    //pack.position = fixPack.position;
    //cocosPlayer -> set(fixPack.position);
    cocosPlayer -> apply_updates(fixPack);
}


void Player::remove() {
    pack.message = "exit";
    cocosPlayer -> remove_self(gameWorld);
}


// saves current position to push updates on server
playerInitPack Player::getPack() {
    pack.position = cocosPlayer -> get_position();
    return pack;
}


//// game mechanics ////

void Player::add_damage(int points) {
    pack.health -= points;
    if (pack.health <= 0) {
        printf("oh fuck I'm dead :(\n");
    }
}


void Player::add_score(int points) {
    pack.score += points;
}
