//
//  HelperStructs.h
//  dragon
//
//  Created by Владимир on 28.04.15.
//
//

#ifndef __dragon__playerInitPack__
#define __dragon__playerInitPack__

#include "cocos2d.h"
#include <stdio.h>
#include <string>
#include <map>
#include <vector>

#define VELOCITY 115
#define HEALTH 100

/** server API interface data structure */
struct playerInitPack {
    // client stuff
    cocos2d::Vec2 position;
    cocos2d::Vec2 target; // clicked zone
    double velx;
    double vely;
    double rotation;
    double angularVel;
    
    std::map<std::string, std::string> body;
    std::string nickname;
    int velocity;
    int health;
    int score;
    
    // server stuff
    std::string publicKey;
    std::string playerID;
    std::string message;
    int chosenSkin;
    int chosenMap;
    std::string gameName;
    std::vector<cocos2d::Vec2> terrain;
    struct gameBlock {
        int x;
        int y;
        int rot;
    };
    std::vector<gameBlock> blocks;
    
    /** basic initialization without body */
    playerInitPack() {
        playerID = strdup("Default");
        message = strdup("Default");
        velocity = VELOCITY;
        health = HEALTH;
        score = 0;
    }
    
    /** basic initialization for opponents by skin number and start position */
    playerInitPack(int skinNum, cocos2d::Vec2 position0) {
        chosenSkin = skinNum;
        body = getSkin(skinNum);
        position = position0; // start positions can differ
        playerID = strdup("Default");
        message = strdup("Default");
        velocity = VELOCITY;
        health = HEALTH;
        score = 0;
    }
    
    /** basic initialization specifying body and start position */
    playerInitPack(std::map<std::string, std::string> body0, cocos2d::Vec2 position0) {
        body = body0;
        position = position0; // start positions can differ
        playerID = strdup("Default");
        message = strdup("Default");
        velocity = VELOCITY;
        health = HEALTH;
        score = 0;
    }
    
    /** starter pack for all players */
    static std::vector<playerInitPack> defaultInitStuff() {
        std::vector<playerInitPack> def;
        std::map<std::string, std::string> body_buffer;
        
        // skin 0
        body_buffer["left"] = "dragon/dragon_white_left.png";
        body_buffer["right"] = "dragon/dragon_white_right.png";
        def.push_back(playerInitPack(body_buffer, cocos2d::Vec2(100, 130)));
        
        // skin 1
        body_buffer["left"] = "dragon/dragon_red_left.png";
        body_buffer["right"] = "dragon/dragon_red_right.png";
        def.push_back(playerInitPack(body_buffer, cocos2d::Vec2(500, 130)));
        
        // ....
        
        return def;
    }
    
    
    static std::map<std::string, std::string> getSkin(int i) {
        std::map<std::string, std::string> body_skin;
        switch (i) {
            case 0:
                body_skin["left"] = "dragon/dragon_white_left.png";
                body_skin["right"] = "dragon/dragon_white_right.png";
                return body_skin;
            case 1:
                body_skin["left"] = "dragon/dragon_red_left.png";
                body_skin["right"] = "dragon/dragon_red_right.png";
                return body_skin;
            default:
                body_skin["left"] = "dragon/dragon_white_left.png";
                body_skin["right"] = "dragon/dragon_white_right.png";
                return body_skin;
        }
    }
    
    static const char* getMap(int i) {
        switch (i) {
            case 0:
                return "Scene1.png\0";
            case 1:
                return "Scene2.png\0";
            default:
                return "Scene1.png\0";
        }
    }
};

#endif
