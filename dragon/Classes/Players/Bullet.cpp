//
//  Bullet.cpp
//  dragon
//
//  Created by Владимир on 16.05.15.
//
//

#include "Bullet.h"

#define BULLET_VELOCITY 400

// how to sent updates about it???

Bullet::Bullet(cocos2d::Vec2 start, cocos2d::Layer* gameScene) {
    // create a picture
    body = cocos2d::Sprite::create("bullet.jpg");
    body -> setPosition(start);
    
    // create a real physical body
    realBody = cocos2d::PhysicsBody::createCircle(body->getContentSize().width/2 - 10, cocos2d::PhysicsMaterial(0, 0.01, 0.01));
    body -> setPhysicsBody(realBody);
    gameScene -> addChild(body, -3);
    
    //realBody -> applyImpulse(count_vector(start, target));
}


Bullet::~Bullet() {
}


/*
// send this pack on server
playerInitPack AbstractPlayer::get_parameters(){
    playerInitPack pack;
    pack.position = realBody -> getPosition();
    pack.rotation = realBody -> getRotation();
    pack.angularVel = realBody -> getAngularVelocity();
    pack.velx = realBody -> getVelocity().x;
    pack.vely = realBody -> getVelocity().y;
    return pack;
}
*/

cocos2d::Vec2 Bullet::count_vector(cocos2d::Vec2 src, cocos2d::Vec2 dest) {
    double gipoten = std::sqrt(1.0*(src.x - dest.x)*(src.x - dest.x) + 1.0*(src.y - dest.y)*(src.y - dest.y));
    double cosV = (dest.x - src.x)/gipoten;
    double sinV = (dest.y - src.y)/gipoten;
    return cocos2d::Vec2(BULLET_VELOCITY*cosV, BULLET_VELOCITY*sinV);
}


/*
// intelligent move for opponent
void AbstractPlayer::apply_updates(playerInitPack pack) {
    body -> setPosition(pack.position);
    body -> setRotation(pack.rotation);
    realBody -> setVelocity(cocos2d::Vec2(pack.velx, pack.vely));
    realBody -> setAngularVelocity(pack.angularVel);
}
*/


void Bullet::move(cocos2d::Vec2 target, cocos2d::Layer* gameScene) {
    cocos2d::Vec2 start = body -> getPosition();
    realBody -> applyImpulse(count_vector(start, target));
}

void Bullet::remove_self(cocos2d::Layer* gameScene) {
    realBody -> removeFromWorld();
    gameScene -> removeChild(body);
    
    // + send callback????
}

void Bullet::check_blocks(cocos2d::Layer* gameScene) {
    cocos2d::Vec2 pos = body -> getPosition();
}
