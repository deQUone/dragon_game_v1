//
//  AbstractPlayer.cpp
//  dragon
//
//  Created by Владимир on 18.04.15.
//
//

#include "AbstractPlayer.h"

/** visible infoLabel offset from sprite object */
#define INFOLABEL_Y_OFFSET 60


AbstractPlayer::AbstractPlayer(playerInitPack pack, cocos2d::Layer* gameScene) {
    bodyStorage = pack.body;
    nickname = pack.nickname;
    velocity = pack.velocity;  // fixed impulse
    
    create(pack.position, bodyStorage["right"], gameScene);  // everyone starts to right?
    vector_x = 1;
    vector_y = 1;
}


AbstractPlayer::~AbstractPlayer() {
    bodyStorage.clear();
}


cocos2d::Vec2 AbstractPlayer::get_position() {
    //printf("ratation=%.2f, angul vel=%.2f, velocity(%.2f %.2f)", realBody -> getRotation(), realBody->getAngularVelocity(), realBody->getVelocity().x, realBody->getVelocity().y);
    return realBody -> getPosition();
}


// send this pack on server
playerInitPack AbstractPlayer::get_parameters(){
    playerInitPack pack;
    pack.position = realBody -> getPosition();
    pack.rotation = realBody -> getRotation();
    pack.angularVel = realBody -> getAngularVelocity();
    pack.velx = realBody -> getVelocity().x;
    pack.vely = realBody -> getVelocity().y;
    return pack;
}


cocos2d::Vec2 AbstractPlayer::count_vector(cocos2d::Vec2 src, cocos2d::Vec2 dest) {    
    double gipoten = std::sqrt(1.0*(src.x - dest.x)*(src.x - dest.x) + 1.0*(src.y - dest.y)*(src.y - dest.y));
    double cosV = (dest.x - src.x)/gipoten;
    double sinV = (dest.y - src.y)/gipoten;
    return cocos2d::Vec2(velocity*cosV, velocity*sinV);
}


// intelligent move for opponent
void AbstractPlayer::apply_updates(playerInitPack pack) {
    body -> setPosition(pack.position);
    body -> setRotation(pack.rotation);
    realBody -> setVelocity(cocos2d::Vec2(pack.velx, pack.vely));
    realBody -> setAngularVelocity(pack.angularVel);
}


void AbstractPlayer::move(int x, int y) {
    cocos2d::Vec2 position = realBody -> getPosition();
    // avatar pivoting logic
    if ((x - position.x)/std::abs(x - position.x) != vector_x) {
        vector_x = (x - position.x)/std::abs(x - position.x);
        if (vector_x == -1) {
            body -> setTexture(bodyStorage["left"]);
        } else if (vector_x == 1) {
            body -> setTexture(bodyStorage["right"]);
        } else {
            printf("WOW SUCH MATH VERY ABS: %i\n", vector_x); // just debug printout...
        }
    }
    if ((y - position.y)/std::abs(y - position.y) != vector_y) {
        vector_y = (y - position.y)/std::abs(y - position.y);
        // + we can redraw sprite if needed :)
    }
    
    target = cocos2d::Vec2(x, y);
    realBody -> applyImpulse(count_vector(position, target));
}


void AbstractPlayer::create(const cocos2d::Vec2 pos, const std::string filename, cocos2d::Layer* gameScene) {
    if (nickname.length() > 0) {
        // create info label
        infoLabel = cocos2d::Label::createWithTTF(nickname, "fonts/PFIsotextPro-Regular.ttf", 25);
        infoLabel -> setPosition(pos.x, pos.y + INFOLABEL_Y_OFFSET);
        gameScene -> addChild(infoLabel, -2);
    }
    // create a picture
    body = cocos2d::Sprite::create(filename);
    body -> setPosition(pos);
    
    // create a real physical body
    realBody = cocos2d::PhysicsBody::createCircle(body->getContentSize().width/2 - 15, cocos2d::PhysicsMaterial(0, 0.01, 0.01));
//    realBody -> setCollisionBitmask(1);
    body -> setPhysicsBody(realBody);
    gameScene -> addChild(body, -3);
}


void AbstractPlayer::set(cocos2d::Vec2 pos) {
    cocos2d::Vec2 curr_pos = realBody -> getPosition();
    if ( std::abs(curr_pos.x - pos.x) > 10 || std::abs(curr_pos.y - pos.y) > 10) {
        if (nickname.length() > 0) {
            infoLabel -> setPosition(pos.x, pos.y + INFOLABEL_Y_OFFSET);
        }
        body -> setPosition(pos);
    }
}


void AbstractPlayer::remove_self(cocos2d::Layer* gameScene) {
    if (nickname.length() > 0) {
        gameScene -> removeChild(infoLabel);
        nickname.clear();
    }
    realBody -> removeFromWorld();
    gameScene -> removeChild(body);
}


void AbstractPlayer::fix_label_pos() {
    cocos2d::Vec2 pos = body -> getPosition();
    if (nickname.length() > 0) {
        infoLabel -> setPosition(pos.x, pos.y + INFOLABEL_Y_OFFSET);
    }
}


void AbstractPlayer::attack(cocos2d::Layer* scene) {
    // create a bullit    
    
    cocos2d::Vec2 pos = body -> getPosition();
    if (vector_x == 1) {
        pos.x += body -> getContentSize().width/2;
    }
    else {
    	pos.x -= body -> getContentSize().width/2;
    }
    Bullet* bullet = new Bullet(pos, scene);
    bullet -> move(cocos2d::Vec2(pos.x + vector_x * 100, pos.y + 10), scene);
    
    	    
    // set author / callback / !! better set playerID ! cause we will draw others bullits too??
    //
    // or! set a flag if author is host. if yes - do updates to superPlayer. if no - do nothing, its author will process it
    
    // push 
}
