//
//  player.h
//  dragon
//
//  Created by Владимир on 28.03.15.
//
//

#ifndef __dragon__player__
#define __dragon__player__

#include "cocos2d.h"

#include "AbstractPlayer.h"
#include "HelperStructs.h"

class Player {
public:
    /** create an interface to a physical game-body */
    Player(playerInitPack, cocos2d::Layer* );
    /** remove the interface to a physical game-body */
    ~Player();
    /** send signal to a physical game-body to change its position */
    void change_pos(cocos2d::Vec2);
    /** create a fireball and push it */
    void attack();
    /** set physical game-body on a current position */
    void fix_pos(cocos2d::Vec2);
    /** fix label movement */
    void move_label();
    /** experiments in synchronizing remove players */
    void simulate_movement(playerInitPack);
    /** remove game-body from the scene */
    void remove();
    /** get interface properties stored in the pack */
    playerInitPack getPack();
    /** game-mechanics method */
    void add_damage(int);
    /** game-mechanics method */
    void add_score(int);
private:
    /** related physical game-body */
    AbstractPlayer* cocosPlayer;
    /** stores current cocos2d scene */
    cocos2d::Layer* gameWorld;
    /** stores all recent physical game-body data */
    playerInitPack pack;
};

#endif /* defined(__dragon__player__) */
