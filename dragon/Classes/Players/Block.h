//
//  bot.h
//  dragon
//
//  Created by Владимир on 01.04.15.
//
//

#ifndef __dragon__bot__
#define __dragon__bot__

#include "cocos2d.h"

class Block {
public:
    Block(int, int, int, const char*, cocos2d::Layer* );
};

#endif /* defined(__dragon__bot__) */
