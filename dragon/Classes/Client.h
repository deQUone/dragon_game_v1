//
//  Client.h
//  dragon
//
//  Created by Владимир on 04.04.15.
//
//

#ifndef __dragon__Client__
#define __dragon__Client__


//#define SERVER "http://127.0.0.1:8000" // test
#define SERVER "http://vksmm.info/game_api"     // production

#include <iostream>
#include <string>
#include <stdio.h>
#include <map>
#include <vector>
#include <thread>
#include <mutex>

#include "curl/curl.h"
#include "external/rapidjson/document.h"
#include "external/rapidjson/writer.h"
#include "external/rapidjson/stringbuffer.h"
#include "Players/HelperStructs.h"

// SINGLETON! //
class Client {
public:
    ~Client(){};
    /** get class instance */
    static Client* getSharedInstance();
    /** 
     @brief     Get status of receiving data
     @return -2     request is in process
     @return -1     error occupied
     @return 0      no request was sent
     @return 1      data received, request complete
     */
    int isRequestDone();
    /** get vector of updates from API server */
    std::vector<playerInitPack> getUpdates();
    /** send request to start a new game */
    void startNewGame(playerInitPack);
    /** send request to get a list of active games */
    void listOfGames();
    /** send request to connect to a concrete game */
    void connectToGame(playerInitPack);
    /** send your current stats on server */
    void sendUpdates(playerInitPack);
    /** load current updates */
    void fetchUpdates(playerInitPack);
    /** send request to exit the game */
    void exitGame(playerInitPack); // outing room/game
private:
    Client();
    /* worker thread stuff */
    void worker();
    /** thread marker */
    int done;
    std::mutex marker_mutex;
    std::string url;
    /** sends request in new thread with CURL library */
    void curlThread(const char* );
    /** buffer string for saving responce */
    std::string curlResp;
    /** parse string into JSON following game built-in action-types */
    void parseResponse(const char* );
    /** parses list into terrain-edges array */
    std::vector<cocos2d::Vec2> parseTerrain(const rapidjson::Value& );
    /** parses list into blocks-array */
    std::vector<playerInitPack::gameBlock> parseBlocks(const rapidjson::Value& );
    /** gets "publicKey" and "playerID" values from JSON */
    playerInitPack message_new_game(const rapidjson::Value& );
    /** gets "name", "publicKey", "online" values from JSON */
    playerInitPack message_list(const rapidjson::Value& );
    /** gets "playerID" value from JSON */
    playerInitPack message_connect(const rapidjson::Value& );
    /** gets "playerID" value from JSON */
    playerInitPack message_exit(const rapidjson::Value& );
    /** gets "playerID", "nickname", "skin", "x", "y" values from JSON */
    playerInitPack message_create(const rapidjson::Value& );
    /** gets "playerID", "curx", "cury", "targx", "targy" values from JSON */
    playerInitPack message_move(const rapidjson::Value& );
    /** gets "message" value from JSON */
    playerInitPack message_error(const rapidjson::Value& );
    /** stores getUpdates-result */
    std::vector<playerInitPack> updates;
};

#endif /* defined(__dragon__Client__) */
