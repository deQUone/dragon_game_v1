//
//  Client.cpp
//  dragon
//
//  Created by Владимир on 04.04.15.
//
//

#include "Client.h"


Client::Client() {
    curl_global_init(CURL_GLOBAL_ALL);
    done = 0;
    
    std::thread wt(&Client::worker, this);
    wt.detach();
}


static Client* instance = nullptr;


Client* Client::getSharedInstance() {
    if (!instance) {
        instance = new Client;
    }
    return instance;
}


int Client::isRequestDone() {
    int buf = done;
    done = 0;
    return buf;
}


void Client::worker() {
    int start = 0;
    while (true) {
        marker_mutex.lock();
        start = done;
        marker_mutex.unlock();
        if (start == 3) {
            curlThread(url.c_str());
            marker_mutex.lock();
            url.clear();
            marker_mutex.unlock();
        }
    }
}


/*
 retrieving data style:
 
 {
    "response": [
    { ... },
      ...,
    { ... }
    ]
 }
 
 
 /// CREATE ///
 
 {
    "type": "create",
    "playerID": "asdop123",
    "skin": 1, 
    "nickname": "ZZZp",
    "x": 400,
    "y": 400,
 
    "health": 90,
    "score": 0
 }
 
 /// MOVE ///
 
 {
    "type": "move",
    "playerID": "asdop123",
    "x": 100,
    "y": 200
 }
 
 /// DEMO - json is static ///
 //const char* json = "{\"response\":[{\"type\": \"create\", \"playerID\": \"asdop123\", \"skin\": 1, \"nickname\": \"ZZZp\", \"x\": 400, \"y\": 400, \"health\": 90, \"score\": 0}, {\"type\": \"create\", \"playerID\": \"oais0131\", \"skin\": 0, \"nickname\": \"IIxxx\", \"x\": 200, \"y\": 450, \"health\": 100, \"score\": 0}, {\"type\": \"move\", \"playerID\": \"asdop123\", \"x\": 600, \"y\": 600}]}";
*/


std::vector<cocos2d::Vec2> Client::parseTerrain(const rapidjson::Value& message){
    std::vector<cocos2d::Vec2> result;
    for (rapidjson::SizeType i = 0; i < message.Size(); ++i) {
        const rapidjson::Value& item = message[i];
        result.push_back(cocos2d::Vec2(item[0].GetInt(), item[1].GetInt()));
    }
    return result;
}


std::vector<playerInitPack::gameBlock> Client::parseBlocks(const rapidjson::Value& message){
    std::vector<playerInitPack::gameBlock> blockList;
    playerInitPack::gameBlock buf;
    for (rapidjson::SizeType i = 0; i < message.Size(); ++i) {
        const rapidjson::Value& item = message[i];
        buf.x = item[0].GetInt();
        buf.y = item[1].GetInt();
        buf.rot = item[2].GetInt();
        blockList.push_back(buf);
    }
    return blockList;
}


playerInitPack Client::message_new_game(const rapidjson::Value& message){
    playerInitPack pack;
    pack.publicKey = message["publicKey"].GetString();
    pack.playerID = message["playerID"].GetString();
    pack.terrain = parseTerrain(message["terrain"]);
    pack.blocks = parseBlocks(message["blocks"]);
    pack.message = strdup("new_game");
    return pack;
}


playerInitPack Client::message_list(const rapidjson::Value& message){
    playerInitPack game_prop;
    game_prop.body["name"] = message["name"].GetString();
    game_prop.body["publicKey"] = message["publicKey"].GetString();
    game_prop.body["online"] = message["online"].GetString();
    game_prop.chosenMap = message["chosenMap"].GetInt();
    game_prop.terrain = parseTerrain(message["terrain"]);
    game_prop.blocks = parseBlocks(message["blocks"]);
    game_prop.message = strdup("list");
    return game_prop;
}


playerInitPack Client::message_connect(const rapidjson::Value& message){
    playerInitPack pack;
    pack.playerID = message["playerID"].GetString();
    pack.message = strdup("connect");
    return pack;
}


playerInitPack Client::message_exit(const rapidjson::Value& message){
    playerInitPack pack;
    pack.playerID = message["playerID"].GetString();
    pack.message = strdup("exit");
    return pack;
}


playerInitPack Client::message_create(const rapidjson::Value& message){
    playerInitPack pack = playerInitPack();
    pack.playerID = message["playerID"].GetString();
    pack.nickname = message["nickname"].GetString();
    pack.body = playerInitPack::getSkin(message["skin"].GetInt());
    pack.position = cocos2d::Vec2(message["x"].GetDouble(), message["y"].GetDouble());
    pack.message = strdup("create");
    return pack;
}


playerInitPack Client::message_move(const rapidjson::Value& message) {
    playerInitPack pack = playerInitPack();
    pack.playerID = message["playerID"].GetString();
    pack.position = cocos2d::Vec2(message["curx"].GetDouble(), message["cury"].GetDouble());
    pack.target = cocos2d::Vec2(message["targx"].GetDouble(), message["targy"].GetDouble());
    // new parameters
    //pack.angularVel = message["angularVel"].GetDouble();
    pack.message = strdup("move");
    return pack;
}


playerInitPack Client::message_error(const rapidjson::Value& message) {
    playerInitPack pack;
    pack.message = message["message"].GetString();
    return pack;
}


void Client::parseResponse(const char* json) {
    rapidjson::Document doc;
    doc.Parse(json);
    const rapidjson::Value& action_list = doc["response"];
    for (rapidjson::SizeType i = 0; i < action_list.Size(); ++i) {
        const rapidjson::Value& message = action_list[i];
        if (strcmp(message["type"].GetString(), "new_game") == 0) {
            updates.push_back(message_new_game(message));
        } else if (strcmp(message["type"].GetString(), "list") == 0) {
            updates.push_back(message_list(message));
        } else if (strcmp(message["type"].GetString(), "connect") == 0) {
            updates.push_back(message_connect(message));
        } else if (strcmp(message["type"].GetString(), "exit") == 0) {
            updates.push_back(message_exit(message));
        } else if (strcmp(message["type"].GetString(), "create") == 0) {
            updates.push_back(message_create(message));
        } else if (strcmp(message["type"].GetString(), "move") == 0) {
            updates.push_back(message_move(message));
        } else if (strcmp(message["type"].GetString(), "error") == 0) {
            updates.push_back(message_error(message));
        }
    }
    printf("Got: %lu - OK\n", updates.size());
}


std::vector<playerInitPack> Client::getUpdates() {
    marker_mutex.lock();
    done = 0;
    std::vector<playerInitPack> buf = updates;
    updates.clear();
    marker_mutex.unlock();
    return buf;
}


void Client::startNewGame(playerInitPack pack){
    marker_mutex.lock();
    url = std::string(SERVER) + "/create_game/?skin=" + std::to_string(pack.chosenSkin) + "&nickname=" + pack.nickname + "&x=" + std::to_string(pack.position.x) + "&y=" + std::to_string(pack.position.y) + "&chosenMap=" + std::to_string(pack.chosenMap) + "&gameName=" + pack.gameName;
    done = 3;
    marker_mutex.unlock();
}


void Client::listOfGames() {
    marker_mutex.lock();
    url = std::string(SERVER) + "/get_games/";
    done = 3;
    marker_mutex.unlock();
}


void Client::connectToGame(playerInitPack pack) {
    marker_mutex.lock();
    url = std::string(SERVER) + "/connect/?skin=" + std::to_string(pack.chosenSkin) + "&nickname=" + pack.nickname + "&x=" + std::to_string(pack.position.x) + "&y=" + std::to_string(pack.position.y) + "&publicKey=" + pack.publicKey;
    done = 3;
    marker_mutex.unlock();
}


// + add action,
// + add message,
// + add recognizing what exactly to send...
void Client::sendUpdates(playerInitPack pack) {
    marker_mutex.lock();
    url = std::string(SERVER) + "/update/?publicKey=" + pack.publicKey + "&playerID=" + pack.playerID + "&curx=" + std::to_string(pack.position.x) + "&cury=" + std::to_string(pack.position.y) + "&targx=" + std::to_string(pack.target.x) + "&targy=" + std::to_string(pack.target.y);
//    + "&health=" + std::to_string(pack.health) + "&score=" + std::to_string(pack.score);
    done = 3;
    marker_mutex.unlock();
}


// we have a phys world so tell all other players about your curent position
void Client::fetchUpdates(playerInitPack pack){
    marker_mutex.lock();
    url = std::string(SERVER) + "/fetch_updates/?publicKey=" + pack.publicKey + "&playerID=" + pack.playerID + "&curx=" + std::to_string(pack.position.x) + "&cury=" + std::to_string(pack.position.y);
    done = 3;
    marker_mutex.unlock();
}


void Client::exitGame(playerInitPack pack) {
    marker_mutex.lock();
    url = std::string(SERVER) + "/exit/?publicKey=" + pack.publicKey + "&playerID=" + pack.playerID;
    done = 3;
    marker_mutex.unlock();
    printf("exiting game...");
}

/// multythreading zone ///

/** count size of upstream data */
size_t write_func(void *ptr, size_t size, size_t count, void *stream) {
    ((std::string*)stream)->append((char*)ptr, 0, size*count);
    return size*count;
}


void Client::curlThread(const char *str) {
    done = -2;
    curlResp.clear();
    char* reqURL = strdup(str);
    for (int i = 0; i < strlen(str); i++) {
        if (str[i] != ' ') {
            reqURL[i] = str[i];
        } else {
            reqURL[i] = '+';
        }
    }
    CURL* curl = curl_easy_init();
    CURLcode res;
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, reqURL);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_func);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &curlResp);
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 3);
        res = curl_easy_perform(curl);
        if (res == CURLE_OK) {
            int p = curlResp.length();
            for (; p >= 0; --p) {
                if (curlResp[p] == '}') {
                    break;
                }
            }
            curlResp[p + 1] = '\0';
            printf("clear responce: %s\n", curlResp.c_str());
            if (p > 4) {
                parseResponse(curlResp.c_str());
                done = 1;
            } else {
                done = -1;
            }
        } else {
            printf("errors: %s\n", curl_easy_strerror(res));
            done = -1;
        }
        curl_easy_cleanup(curl);
    } else {
        printf("no curl((\n");
        done = -1;
    }
    free(reqURL);
}
