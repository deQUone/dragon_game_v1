# coding: utf8
import requests 
import json
import time
import math

GRAVITY = -98

class DragonPlayer(object):
	"""docstring for ClassName"""
	def __init__(self, nickname, skin, x, y):
		super(DragonPlayer, self).__init__()
		self.nickname = nickname
		self.skin = skin
		self.x = x
		self.y = y
		self.velocity = 115
		'''
		self.velX = 0.0
		self.velY = 0.0
		self.angularVel = 0.0
		self.rotation = 0.0
		'''
		self.domen = "http://vksmm.info"
		#self.domen = "http://127.0.0.1:8000"
		self.publicKey = ""
		self.playerID = ""

	""" game test methods """
	def test_game(self):
		try:
			game = self.list_of_games()[0]
			self.publicKey = game["publicKey"]
			if self.connect():
				print "Connected - OK"
				time.sleep(1)
			
				self.simulate([100, 300, 2])
				
				self.simulate([300, 400, 1])

				self.simulate([100, 300, 1])

				time.sleep(3)

				self.exit_game()
			else:
				print "Problems occupy"
		except IndexError:
			print "No active games at the moment"

	def simulate(self, dot):
		print "Moving..."
		self.send_updates({"targx": dot[0], "targy": dot[1]})
		self.x, self.y = self.guess_route(dot)
		time.sleep(dot[2])

	# next_x, next_y, dt
	def guess_route(self, coordinates):
		gipotenuza = math.sqrt((self.x - coordinates[0])*(self.x - coordinates[0]) + (self.y - coordinates[1])*(self.y - coordinates[1]))
		cosV = (coordinates[0] - self.x)/gipotenuza
		sinV = (coordinates[1] - self.y)/gipotenuza
		new_x = self.x + self.velocity*cosV*coordinates[2]
		new_y = self.y + self.velocity*sinV*coordinates[2] + GRAVITY*coordinates[2]*coordinates[2]/2
		if new_y < 0:
			new_y = 15  # sprite centre
		#print "New: {0}, {1}\ngipot={2}\ncos={3}, sin={4}\n----------------------".format(new_x, new_y, gipotenuza, cosV, sinV)
		return new_x, new_y

	""" API methods """
	def list_of_games(self):
		endpoint = "get_games/"
		return self.touchAPI(endpoint, None)

	def connect(self):
		endpoint = "connect"
		params = {
			"publicKey": self.publicKey,
			"nickname": self.nickname,
			"skin": self.skin,
			"x": self.x,
			"y": self.y
		}
		response = self.touchAPI(endpoint, params)
		for item in response:
			if item["type"] == "connect":
				self.playerID = item["playerID"]
				return True
		return False
		

	def send_updates(self, updates):
		endpoint = "update"
		updates["curx"] = self.x
		updates["cury"] = self.y
		updates["publicKey"] = self.publicKey
		updates["playerID"] = self.playerID
		self.touchAPI(endpoint, updates)
		print "Updates sent - OK"

	def exit_game(self):
		endpoint = "exit"
		params = {
			"publicKey": self.publicKey,
			"playerID": self.playerID
		}
		res = self.touchAPI(endpoint, params)
		print "Exited game - OK"

	def touchAPI(self, endpoint, params):
		url = "{0}/{1}".format(self.domen, endpoint)
		if params:
			response = requests.get(url, params=params)
		else:
			response = requests.get(url)
		try:
			return response.json()["response"]
		except KeyError:
			return response.json()
		except ValueError:
			return None


if __name__ == "__main__":
	bot1 = DragonPlayer("bot1", 1, 400, 30)
	bot1.test_game()
